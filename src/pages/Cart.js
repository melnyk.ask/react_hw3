import Card from "../components/Card";
import "./Favorite.scss";

function Cart({ onclick, onFavClick, favRemove, starAdded, show_add_to_cart_btn, delFromCart }) {
    if (localStorage.getItem("products")) {

        let products = JSON.parse(localStorage.getItem("products"));
        console.log("------>>", products);

        
    
        return (
            <div className="favorite_page">
                <div className="favorite_page_wrap">
                    { products.wheels.map(
                                        (card) => {
                                            if (localStorage.getItem(`cart_id${card.id}`)==="1") { 
                                                return (<Card key={card.id}
                                                    id={card.id}
                                                    color={card.color}
                                                    url={card.url}
                                                    title={card.title}
                                                    price={card.price}
                                                    onclick={() => { }}
                                                    onFavClick={onFavClick}
                                                    favRemove={favRemove}
                                                    starAdded={starAdded}
                                                    show_add_to_cart_btn={false}
                                                    del_from_cart={true}
                                                    delFromCart={delFromCart}
                                                    />);
                                            }                
                                        }
                    )}
                </div>                
            </div>
            );
    }
    else return null;
}

export default Cart;