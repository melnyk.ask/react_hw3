// import React from "react";
import "./Button.scss";

function Button(props) {
    

       return (
        <>
            <button
                className="button"
                id={props.id}
                style={{ backgroundColor: props.backgroundColor }}
                onClick={props.onClick}>
                    {props.text}
            </button>
        </>
    ) 
}

export default Button;