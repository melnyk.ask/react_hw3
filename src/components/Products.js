import Card from "./Card";
import "./Products.scss";

function Products({ products,
    onclick,
    onFavClick,
    addToCart,
    delFromCart,
    favRemove,
    starAdded,
    show_add_to_cart_btn,
    del_from_cart,
    test, text,
    header,
    closeButton,
    actions,
    closeModal,
    visible1,
    shadow,
    show_fav_star }) {
    function out () { 
        if (products.wheels) { 
        return (products.wheels.map(
            (card) => {
                return (<Card key={card.id}
                    id={card.id}
                    color={card.color}
                    url={card.url}
                    title={card.title}
                    price={card.price}
                    onclick={onclick}
                    onFavClick={onFavClick}
                    favRemove={favRemove}
                    starAdded={starAdded}
                    show_add_to_cart_btn={show_add_to_cart_btn}
                    del_from_cart={del_from_cart}
                    addToCart={addToCart}
                    delFromCart={delFromCart}
                    //
                    test={test}
                    text={text}
                    header={header}
                    closeButton={closeButton}
                    actions={actions}
                    closeModal={closeModal}
                    visible1={visible1}
                    shadow={shadow}
                    show_fav_star={ show_fav_star}
                    //
            />);
            }
               ))
        }
        else return null;
    }

    return (
        <div className="products" >
            {out()} 
        </div>
    )    
}

export default Products;