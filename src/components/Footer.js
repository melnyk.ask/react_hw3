// import React from "react";
import "./Footer.scss";

function Footer() {
   
    return (
    <div className="footer" >
        <p className="footer_title1">WHEELS and RIMS for your car</p>
        <p className="footer_title2">@ 2023 by MAD | All Rights Reserved</p>            
    </div>
    )    
}

export default Footer;