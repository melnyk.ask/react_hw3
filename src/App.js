// import Products from "./components/Products.js";
// import Modal from "./components/Modal.js";
// import Footer from "./components/Footer.js";
// import './App.scss';
// import star from "./img/star.png";
// import cart from "./img/cart.png";
// import { useEffect, useState } from "react";

// function App() {  

//   let [products, setProducts] = useState([]);
//   let [add_to_cart, set_add_to_cart] = useState(0);
//   let [add_to_fav, set_add_to_fav] = useState(0);
//   let [add_to_fav_id, set_add_to_fav_id] = useState("");
//   let [star_added, set_star_added] = useState(0);
//   let [visible1, set_visible1] = useState(false);
//   let [shadow, set_shadow] = useState(false);

// let actions = <div className="actions">
//                 <div className="ok" onClick={addToCart}>Add to cart</div>
//                 <div className="cancel" onClick={closeModal1}>Cancel</div>
//               </div>;
    
  
//   function showModal() {
//     set_visible1(true);
//     set_shadow(true);
//   }

//   function addToCart () {
//     let increm = add_to_cart;
//     set_add_to_cart(increm + 1);     
      
//     localStorage.setItem("in_cart", add_to_cart);
//       // localStorage.setItem(`cart_id${id}`, true);
//       closeModal1();
//   }
  
//   function addToFav(id) { 
//     let increm = add_to_fav;
//     set_add_to_fav(increm + 1);
        
//     localStorage.setItem("in_fav", add_to_fav);

//     set_star_added(id);
//   }
  
//   function removeFromFav(id) { 
//     let increm = add_to_fav;

//       // if (!localStorage.getItem(`fav_id${id}`)) { 
//     set_add_to_fav(increm - 1);
        
//     localStorage.setItem("in_fav", add_to_fav);
//       // }

//     set_star_added(id);
 
//   }
  
//   function test(e) {
//     // console.log(e.target.id);
//     if (e.target.id === "test") { 
//       set_visible1(false);
//       set_shadow(false);
//     }
//   }
  
//   function closeModal1() { 
//     set_visible1(false);
//     set_shadow(false);
//   }

//   useEffect(() => { 
//     async function fetchJson() {
//       let request = await fetch("./products.json");
//       let response = await request.json();
//       setProducts(response);
//     }
//     fetchJson();

//     set_add_to_cart(Number(localStorage.getItem("in_cart")));
//     set_add_to_fav(Number(localStorage.getItem("in_fav")));
//     set_add_to_fav_id(localStorage.getItem("in_fav_id")); 

//     // console.log("once");
//   }, []);
  
//   useEffect(() => {   
//     // console.log("update");
//     localStorage.setItem("in_cart", add_to_cart);  
//     localStorage.setItem("in_fav", add_to_fav);
//     localStorage.setItem("in_fav_id", add_to_fav_id);
//   });
  
//     return (
//         <div className="App" onClick={test} id="test">
//             <div className="main_header">
//               <div className="main_header_in">
//                 <div className="main_header_title">
//                   <p className="main_header_title_1">WHEELS</p>
//                   <p className="main_header_title_2">and</p>
//                   <p className="main_header_title_3">RIMS</p>
//                   <p className="main_header_title_4">FOR YOUR CAR</p>
//                 </div>
//                 <div className="cart">
//                 <img className="cart_img" src={cart} alt="#" />
//                 <p className="cart_number ">{add_to_cart}</p>
//               </div>
//               <div className="favorite">
//                 <img className="favorite_img" src={star} alt="#"/>
//                 <p className="favorite_number ">{add_to_fav}</p>
//               </div>
//               </div>
//             </div>
//           {shadow && <div className="shadow" onClick={test} id="test"></div>}
//           {visible1 && <Modal
//             text="Are you sure you want to add this wheel to cart?"
//             header="ADD PRODUCT TO CART"
//             closeButton={true}
//             actions={actions}
//             closeModal={closeModal1}
//               id={1} />}
//             <Products products={products} onclick={showModal} onFavClick={addToFav} favRemove={removeFromFav} starAdded={star_added} />
//             <Footer />
//         </div>
//       );   
//     }

// export default App;
import { Routes, Route } from "react-router-dom";
import Nav from "./pages/Nav";
import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Favorite from "./pages/Favorite";
import NoPage from "./pages/NoPage";
import Products from "./components/Products.js";
import Modal from "./components/Modal.js";
import Footer from "./components/Footer.js";
import './App.scss';
import star from "./img/star.png";
import cart from "./img/cart.png";
import { useEffect, useState } from "react";

function App() {  

  let [products, setProducts] = useState([]);
  let [add_to_cart, set_add_to_cart] = useState(0);
  let [add_to_fav, set_add_to_fav] = useState(0);
  let [add_to_fav_id, set_add_to_fav_id] = useState("");
  let [star_added, set_star_added] = useState(0);
  let [visible1, set_visible1] = useState(false);
  let [shadow, set_shadow] = useState(false);

let actions = <div className="actions">
                <div className="ok" onClick={addToCart}>Add to cart</div>
                <div className="cancel" onClick={closeModal1}>Cancel</div>
              </div>;

  let show_add_to_cart_btn = true;
  let del_from_cart = false;
  let show_fav_star = true;
  
  function showModal() {
    set_visible1(true);
    set_shadow(true);
  }

  function addToCart () {
    let increm = add_to_cart;
    set_add_to_cart(increm + 1);   
    // console.log("ID from addToCart", id);
      
    localStorage.setItem("in_cart", add_to_cart);
      // localStorage.setItem(`cart_id${id}`, true);
      closeModal1();
  }

  function delFromCart () {
    let increm = add_to_cart;
    set_add_to_cart(increm - 1);   
    // console.log("ID from addToCart", id);
      
    localStorage.setItem("in_cart", add_to_cart);
      // localStorage.setItem(`cart_id${id}`, true);
  }
  
  function addToFav(id) { 
    let increm = add_to_fav;
    set_add_to_fav(increm + 1);
        
    localStorage.setItem("in_fav", add_to_fav);

    set_star_added(id);
  }
  
  function removeFromFav(id) { 
    let increm = add_to_fav;

      // if (!localStorage.getItem(`fav_id${id}`)) { 
    set_add_to_fav(increm - 1);
        
    localStorage.setItem("in_fav", add_to_fav);
      // }

    set_star_added(id);
 
  }
  
  function test(e) {
    // console.log(e.target.id);
    if (e.target.id === "test") { 
      set_visible1(false);
      set_shadow(false);
    }
  }
  
  function closeModal1() { 
    set_visible1(false);
    set_shadow(false);
  }

  useEffect(() => { 
    async function fetchJson() {
      let request = await fetch("./products.json");
      let response = await request.json();
      setProducts(response);
      localStorage.setItem("products", JSON.stringify(response));
    }
    fetchJson();

    set_add_to_cart(Number(localStorage.getItem("in_cart")));
    set_add_to_fav(Number(localStorage.getItem("in_fav")));
    set_add_to_fav_id(localStorage.getItem("in_fav_id")); 

    // console.log("once");
  }, []);
  
  useEffect(() => {   
    // console.log("update");
    localStorage.setItem("in_cart", add_to_cart);  
    localStorage.setItem("in_fav", add_to_fav);
    localStorage.setItem("in_fav_id", add_to_fav_id);
  });


  
  return (
    <>
      <Nav />
      <Routes>
        <Route path="/" index element={<Home
          test={test}
          star={star}
          cart={cart}
          add_to_cart={add_to_cart}
          add_to_fav={add_to_fav}
          actions={actions}
          closeModal1={closeModal1}
          products={products}
          showModal={showModal}
          addToFav={addToFav}
          addToCart={addToCart}
          delFromCart={delFromCart}
          removeFromFav={removeFromFav}
          star_added={star_added}
          shadow={shadow}
          visible1={visible1}
          show_add_to_cart_btn={show_add_to_cart_btn}
          del_from_cart={del_from_cart}
          show_fav_star={show_fav_star}
        />} />
        <Route path="/favorite" element={<Favorite
          show_add_to_cart_btn={show_add_to_cart_btn}
          onclick={showModal}
          onFavClick={addToFav}
          favRemove={removeFromFav}
          starAdded={star_added}
          show_fav_star={show_fav_star}
        />} />
        <Route path="/cart" element={<Cart
          del_from_cart={del_from_cart}
          addToCart={addToCart}
          delFromCart={delFromCart}
          show_fav_star={ show_fav_star}
        />} />
        <Route path="/*" element={ <NoPage />} />
      </Routes>
    </>
  );
}

export default App;